from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('about/', views.AboutView.as_view(), name='about'),
    path('create/', views.create_ropa, name='create'),
    path('ropa/<pk>/', views.RopaDetailView.as_view(), name='ropa-detail'),
    path('edit/<pk>/', views.RopaEditView.as_view(), name='ropa-edit'),
    path('ropa/<pk>/create', views.create_ropainstance, name='ropainstance-create'),
    path('ropa/<pk>/delete', views.RopaDeleteView.as_view(), name='ropa-delete'),
    path('ropa/instance/<pk>', views.RopaInstanceDetailView.as_view(), name='ropainstance-detail'),
    path('ropa/instance/<pk>/delete', views.RopaInstanceDeleteView.as_view(), name='ropainstance-delete'),
    path('ropa/instance/<pk>/transfer', views.transfer_stock, name='transfer-stock'),
    path('ropa/instance/<pk>/return', views.return_ropa, name='mark-returned'),
    path('search/', views.search, name='search')
]
