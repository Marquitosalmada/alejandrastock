from django import forms
from .models import Ropa, Color, Talle
from django.contrib.auth.models import User, Group


class RopaModelForm(forms.ModelForm):
    class Meta:
        model = Ropa
        fields = ['cod', 'name', 'categoria', 'precio', 'desc', 'image']


class RopaEditForm(forms.ModelForm):
    class Meta:
        model = Ropa
        fields = ['name', 'precio', 'categoria', 'desc', 'image']


class RopaInstanceCreateForm(forms.Form):
    color = forms.ModelChoiceField(queryset=Color.objects.all(), help_text='Elige un color para tu stock')
    talle = forms.ModelChoiceField(queryset=Talle.objects.all(), help_text='Elige un talle para tu stock')
    posesion = forms.ModelChoiceField(queryset=User.objects.all(), help_text='Elige quien tiene tu stock')
    stock = forms.IntegerField(max_value=50, min_value=1, help_text='Elige cuanto stock tienes')



class RopaTransferForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all(), help_text='Elige a que usuario deseas transferir la ropa')
    stock = forms.IntegerField(max_value=50, min_value=1)


class RopaReturnForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.filter(groups__name='administrador'), help_text='Elige al administrador que le devuelves la ropa')
    stock = forms.IntegerField(max_value=50, min_value=1, help_text='Elige a quien le devuelves la ropa')
    ROPA_ESTADO = (
        ('v', 'Vendida'),
        ('d', 'Devuelta'),
    )
    estado = forms.ChoiceField(choices=ROPA_ESTADO, initial='v', help_text='Elige si marcas el stock como vendido o como devuelto')
