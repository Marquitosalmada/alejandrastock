from django.shortcuts import render
from django.views import generic
from django.http import HttpResponseRedirect
from .models import Ropa, RopaInstance
from .forms import RopaModelForm, RopaEditForm, RopaInstanceCreateForm, RopaTransferForm, RopaReturnForm
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.models import User


class IndexView(generic.TemplateView):
    def get(self, request, **kwargs):
        ropa_list = Ropa.objects.order_by('cod')
        context = {'ropa_list': ropa_list}
        return render(request, 'mainApp/index.html', context)


class AboutView(generic.TemplateView):
    template_name = "mainApp/about.html"


class RopaDetailView(generic.DetailView):
    model = Ropa

    def get_context_data(self, **kwargs):
        context = super(RopaDetailView, self).get_context_data(**kwargs)
        query = RopaInstance.objects.filter(ropa=self.kwargs['pk']).order_by('id')
        lista = []
        tupledup = []
        for ropa in query:
            ropatup = (ropa.color, ropa.talle, ropa.estado, ropa.posesion)
            if ropatup not in tupledup:
                lista.append(ropa)
                tupledup.append(ropatup)
        context['lista'] = lista
        return context


class RopaEditView(UpdateView):
    model = Ropa
    fields = ['name', 'precio', 'categoria', 'desc', 'image']


class RopaDeleteView(DeleteView):
    model = Ropa
    success_url = reverse_lazy('index')


class RopaInstanceCreateView(CreateView):
    model = RopaInstance
    fields = ['posesion', 'talle', 'color']

    def form_valid(self, form):
        form.instance.ropa = Ropa.objects.get(pk=self.kwargs['pk'])
        return super(RopaInstanceCreateView, self).form_valid(form)


class RopaInstanceDetailView(generic.DetailView):
    model = RopaInstance


class RopaInstanceDeleteView(DeleteView):
    model = RopaInstance
    success_url = reverse_lazy('index')


def create_ropa(request):
    form = RopaModelForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
        return HttpResponseRedirect('/')
    else:
        return render(request, 'mainApp/create.html', {'form': form})


def create_ropainstance(request, pk):
    form = RopaInstanceCreateForm(request.POST or None)
    instance_bulk = []
    if request.method == 'POST':
        if form.is_valid():
            for i in range(0, form.cleaned_data['stock']):
                instance = RopaInstance()
                instance.ropa = Ropa.objects.get(cod=pk)
                instance.color = form.cleaned_data['color']
                instance.talle = form.cleaned_data['talle']
                instance.posesion = form.cleaned_data['posesion']
                instance_bulk.append(instance)
            RopaInstance.objects.bulk_create(instance_bulk)
        return HttpResponseRedirect('/')
    else:
        return render(request, 'mainApp/ropainstance_form.html', {'form': form})


def edit_ropa(request, pk):
    form = RopaEditForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        form.cod = pk
        if form.is_valid():
            form.save()
        return HttpResponseRedirect('/')
    else:
        return render(request, 'mainApp/ropa_edit.html', {'form': form})


def search(request):
    ropa_list = Ropa.objects.all()
    ropa_filter = RopaFilter(request.GET, queryset=ropa_list)
    return render(request, 'mainApp/ropa_search.html', {'filter': ropa_filter})


def transfer_stock(request, pk):
    form = RopaTransferForm(request.POST or None)
    instance = RopaInstance.objects.get(pk=pk)
    if request.method == 'POST':
        if form.is_valid():
            query = RopaInstance.objects.all()
            lista = []
            for ropa in query:
                if ropa.color == instance.color and ropa.talle == instance.talle and ropa.estado == instance.estado and ropa.ropa == instance.ropa:
                    lista.append(ropa.id)
            if form.cleaned_data['stock'] > instance.get_copies_count():
                return HttpResponseRedirect('/')
            else:
                modify = lista[:form.cleaned_data['stock']]
                RopaInstance.objects.filter(id__in=modify).update(estado='e', posesion=form.cleaned_data['user'])
                return HttpResponseRedirect('/')
    else:
        return render(request, 'mainApp/ropa_transfer.html', {'form': form, 'ropa': instance})


def return_ropa(request, pk):
    form = RopaReturnForm(request.POST or None)
    instance = RopaInstance.objects.get(id=pk)
    if request.method == 'POST':
        if form.is_valid():
            query = RopaInstance.objects.all()
            lista = []
            instancetuple = (instance.color, instance.talle, instance.estado, instance.posesion)
            for ropa in query:
                ropatuple = (ropa.color, ropa.talle, ropa.estado, ropa.posesion)
                if ropatuple == instancetuple:
                    lista.append(ropa.id)
            if form.cleaned_data['stock'] > instance.get_copies_count():
                return HttpResponseRedirect('/')
            else:
                modify = lista[:form.cleaned_data['stock']]
                RopaInstance.objects.filter(id__in=modify).update(estado=form.cleaned_data['estado'], posesion=form.cleaned_data['user'])
                return HttpResponseRedirect('/')
    return render(request, 'mainApp/ropa_return.html', {'form': form, 'ropa': instance})
