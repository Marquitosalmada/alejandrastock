from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
import uuid


class Ropa(models.Model):
    cod = models.CharField("codigo de producto", primary_key=True, max_length=7)
    name = models.CharField(max_length=200)
    precio = models.FloatField(help_text='Precio de tu producto', null=True)
    categoria = models.ForeignKey('Categoria', help_text='Elige la Categoria de tu producto', on_delete=models.SET_NULL, null=True)
    desc = models.CharField(max_length=200)
    stock = models.IntegerField(verbose_name="Stock del producto", help_text='El stock existente del producto', null=True)
    image = models.ImageField(upload_to='ropa_image', blank=True)

    def __str__(self):
        return "COD: %s NAME: %s" % (self.cod, self.name)

    def get_absolute_url(self):
        return reverse('ropa-detail', args=[self.cod])

    def get_edit_url(self):
        return reverse('ropa-edit', args=[self.cod])


class RopaInstance(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="ID unica para cada copia de la ropa")
    ropa = models.ForeignKey(Ropa, on_delete=models.SET_NULL, null=True)
    posesion = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    talle = models.ForeignKey('Talle', on_delete=models.SET_NULL, null=True, help_text="Selecciona un talle para tu ropa")
    color = models.ForeignKey('Color', on_delete=models.SET_NULL, null=True, help_text="Selecciona un color para tu ropa")

    ROPA_ESTADO = (
        ('a', 'En posesion del admin'),
        ('e', 'Entregada'),
        ('v', 'Vendida'),
        ('d', 'Devuelta'),
    )
    estado = models.CharField(max_length=1, choices=ROPA_ESTADO, blank=True, default='a', help_text='Estado de la ropa')

    class Meta:
        ordering = ["ropa"]
        permissions = (
                        ("can_mark_returned", "Poner ropa como devuelta"),
                        ("can_create_ropa", "Puede crear ropa"),
                      )

    def __str__(self):
        return "Instancia de %s, en posesion de %s, talle %s, color %s" % (self.ropa, self.posesion, self.talle, self.color)

    def get_absolute_url(self):
        return reverse('ropainstance-detail', args=[self.id])

    def get_copies_count(self):
        copies = RopaInstance.objects.filter(estado=self.estado, talle=self.talle, color=self.color, posesion=self.posesion).count()
        return copies


class Color(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Talle(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Categoria(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
