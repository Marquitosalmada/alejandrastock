from .forms import RopaForm
from .models import Ropa

# Create your tests here.

form = RopaForm(cod=1223, name="Ropa test", desc="ROPA DESCRIPTIVA DE TESTEO")

r = Ropa(form.cod, form.name, form.desc)
r.save()


def filtrar_query(queryset):
    newquery = []
    colordup = []
    talledup = []
    for ropa in queryset:
        if ropa.color not in colordup and ropa.talle not in talledup:
            newquery.append(ropa)
            colordup.append(ropa.color)
            talledup.append(ropa.talle)
    return newquery

queryset = RopaInstance.objects.order_by('id')
