from django.contrib import admin
from .models import Ropa, RopaInstance, Talle, Color, Categoria
# Register your models here.

admin.site.register(Ropa)
admin.site.register(RopaInstance)
admin.site.register(Talle)
admin.site.register(Color)
admin.site.register(Categoria)
